from pdb import post_mortem
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from recipes.models import Recipe
from django.contrib.auth.mixins import LoginRequiredMixin


from recipes.forms import RecipeForm
from recipes.models import Recipe


# page_number = request.GET.get("page")
# page_obj = paginator.get_page(page_number)
# return render(request, "list.html", {"page_obj": page_obj})


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeUpdateView(UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context["rating_form"] = RatingForm()
        return context


class RecipeListView(ListView):
    paginate_by = 3
    model = Recipe
    template_name = "recipes/list.html"


class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
